FROM alpine:3.21@sha256:a8560b36e8b8210634f77d9f7f9efd7ffa463e380b75e2e74aff4511df3ef88c

ENV POSTSRSD_SRS_DOMAIN=""

COPY files/ /

# renovate: datasource=repology depName=alpine_3_21/postsrsd depType=dependencies versioning=loose
ARG POSTSRSD_VERSION="2.0.10-r0"

RUN apk add --no-cache \
        postsrsd="${POSTSRSD_VERSION}" \
        supervisor~=4 \
        inotify-tools~=4 \
        iproute2~=6 \
        bash~=5 \
        run-parts~=4 \
    && apk upgrade --no-cache \
        # various CVEs fixed in 3.0.8
        libssl3 libcrypto3

HEALTHCHECK --interval=5s --timeout=5s --start-period=15s CMD "/usr/local/sbin/probe.sh"

ENTRYPOINT ["/usr/local/sbin/docker-entrypoint.sh"]
CMD ["/usr/local/sbin/supervisord.sh"]
