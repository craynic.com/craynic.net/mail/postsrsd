#!/usr/bin/env bash

set -Eeuo pipefail

# run all init scripts
run-parts --exit-on-error --regex='\.sh$' -- "/docker-init.d"

ENVS=(
  POSTSRSD_SRS_DOMAIN
)

# cleanup ENV variables
for e in "${ENVS[@]}"; do
  unset "$e"
done

# run
exec "$@"
