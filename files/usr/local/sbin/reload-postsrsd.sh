#!/usr/bin/env bash

set -Eeuo pipefail

PID_FILE="/run/postsrsd.pid"

# restart PostSRSd
[[ -f "$PID_FILE" ]] && kill -HUP -- "$(cat "$PID_FILE")"
