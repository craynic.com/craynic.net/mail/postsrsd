#!/usr/bin/env bash

set -Eeuo pipefail

CONFD_DIR="/etc/postsrsd/"

inotifywait-dir.sh "$CONFD_DIR" | while read -r line; do
  echo "$line"

  /usr/local/sbin/reload-postsrsd.sh
done
