#!/usr/bin/env bash

set -Eeuo pipefail

if [[ -z "$POSTSRSD_SRS_DOMAIN" ]]; then
  echo "SRS domain not set (env:POSTSRSD_SRS_DOMAIN)..."
  exit 1
fi

CFG_FILE="/etc/postsrsd/postsrsd.conf"

sed -i "/^srs-domain[[:space:]]*=/c\\srs-domain = \"$POSTSRSD_SRS_DOMAIN\"" -- "$CFG_FILE"
